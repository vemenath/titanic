import pandas as pd
import math


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df_Mr = df[df['Name'].str.contains("Mr. ")]
    df_Mrs = df[df['Name'].str.contains("Mrs. ")]
    df_Miss = df[df['Name'].str.contains("Miss. ")]

    # missed values
    na_mr = df_Mr['Age'].isna().sum()
    na_mrs = df_Mrs['Age'].isna().sum()
    na_miss = df_Miss['Age'].isna().sum()

    # mean values
    values_to_match = ["Mr. ", "Master. "]
    mean_mr = df[df['Name'].str.contains("Mr\.|Master\.", case=False, regex=True)]['Age'].mean()
    mean_mrs = df_Mrs['Age'].mean()
    mean_miss = df_Miss['Age'].mean()

    result = [('Mr.', na_mr, math.floor(mean_mr)), ('Mrs.', na_mrs, math.floor(mean_mrs)),
              ('Miss.', na_miss, math.floor(mean_miss))]
    return result
